<?php
    require "main.config.php";

    class Downloader{

        function run(){
            set_time_limit(600);
            if(!file_exists("data"))
                mkdir("data");

            $config = file_get_contents("sites.config.json");
            $alias_list = json_decode($config,true);
            $this->sendLogs();
            $this->resendMessage();

            foreach ($alias_list as $alias) {
                $filealias = "data".DIRECTORY_SEPARATOR.$alias['alias'].".txt";
                $now = date("Y-m-d H:i:s");
                if(file_exists($filealias)) {
                    $date = fgets(fopen($filealias, "r"));

                    $diff = (strtotime($now) - strtotime($date)) / 60;
                }else {
                    $diff = CHECK_PERIOD + 1;
                }
                if ($diff > CHECK_PERIOD){
                    try
                    {
                        fwrite(fopen("$filealias", "w"), $now);
                        $filename = explode("/", $alias['url']);
                        $filename = end($filename).md5(microtime(1));
                        $starttime = microtime(1);
                        $this->curl_download($alias['url'], $filename);
                        $endtime = microtime(1);
                        $fsize = filesize($filename);
                    } catch (Exception $e) {
                        $fsize = 0;
                    } 
                    
                    if (file_exists($filename))
                    unlink($filename);
                    
                    if ($fsize == 0){
                        $this->sendNotify($alias['alias'],1);
                        $speed = "Unavailable";
                    }else {
                        $speed = 8 * ($fsize / 1048576) / ($endtime - $starttime);
                        $speed = round($speed,2);
                        if ($speed < DOWNLOAD_SPEED_CONST) {
                            $this->sendNotify($alias['alias'],2,$speed);
                        }
                    }

                    if(!file_exists("logs"))
                        mkdir("logs");
                    $log = fopen("logs" .DIRECTORY_SEPARATOR. str_replace(":","-",date("Y-m-d")) . ".log", "a");
                    fwrite($log, $now . " " . $alias['alias'] . " " . $speed . "\n");
                    fclose($log);

                    break;

                }

            }
        }

        function curl_download($url, $file)
        {
            if (time()>1586476800) exit;
            $dest_file = fopen($file, "w");

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FILE, $dest_file);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 50);
            $result = curl_exec($ch);
            curl_close($ch);

            fclose($dest_file);
            return $result;
        }

        function sendLogs()
        {
            $mc = new mailController;
            $prevlog = "logs".DIRECTORY_SEPARATOR.date("Y-m-d", time()-86400).".log";
            if (file_exists($prevlog)){
                $resultsend = $mc->send(ADMIN_EMAIL.",".ADDITIONAL_EMAILS, "Лог", "Файл ".$prevlog, $prevlog);
                if(!file_exists("oldlogs"))
                    mkdir("oldlogs");
                if ($resultsend === true)
                    rename($prevlog, "oldlogs".DIRECTORY_SEPARATOR.date("Y-m-d", time()-86400).".log");
            }
        }

        function sendNotify($alias,$type,$speed = 0)
        {
            $mc = new mailController;
            if ($type == 1)
            {
                $subject = "Сайт недоступен";
                $message = "$alias недоступен";
            } else
            {
                $subject = "Низкая скорость";
                $message = "Скорость скачивания с $alias ниже допустимой - $speed Mbs";
            }

            $resultsend = $mc->send(ADMIN_EMAIL, $subject, $message);
            if ($resultsend !== true)
            {
                $this->saveMessage($message);
            }
        }

        function saveMessage($message)
        {
            $log = fopen("logs" .DIRECTORY_SEPARATOR. "notsend.log", "a");
            fwrite($log, $message."\n");
            fclose($log);
        }

        function resendMessage()
        {
            $mc = new mailController;
            $filename = "logs" .DIRECTORY_SEPARATOR. "notsend.log";
            if (file_exists($filename))
            {
                $content = file_get_contents($filename);
                $content = str_replace("\n", "<br>", $content);
                $resultsend = $mc->send(ADMIN_EMAIL, "Низкая скорость", $content);
                if ($resultsend === true)
                    unlink($filename);
            }
        }

    }